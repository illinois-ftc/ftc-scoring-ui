import 'package:scoring_client/main_app.dart';
import 'package:scoring_client/ui/ui.dart';
import 'package:polymer/polymer.dart';
import 'package:polymer_elements/iron_flex_layout/classes/iron_flex_layout.dart';
import 'package:json_java_converter/json_java_converter.dart';
import 'package:logging/logging.dart';

/// [MainApp] used!
main() async {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.time} [${rec.level}] ${rec.loggerName} - ${rec.message}');
  });

  registerJsonConverters();

  await initPolymer();
}
