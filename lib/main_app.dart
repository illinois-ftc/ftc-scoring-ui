@HtmlImport('main_app.html')
library scoring_client.lib.main_app;		

import 'dart:html' as html;

import 'package:polymer_elements/paper_input.dart';
import 'package:polymer/polymer.dart';
import 'package:web_components/web_components.dart';
import 'package:polymer_elements/iron_flex_layout/classes/iron_flex_layout.dart';
import 'package:polymer_elements/iron_pages.dart';
import 'package:polymer_elements/paper_button.dart';
import 'package:polymer_elements/paper_dialog.dart';
import 'package:polymer_elements/paper_toolbar.dart';
import 'package:polymer_elements/paper_header_panel.dart';
import 'package:polymer_elements/paper_tabs.dart';
import 'package:custom_elements/vaadin_grid.dart';
import 'package:logging/logging.dart';
import 'package:scoring_client/services/services.dart';

/// Uses [PaperInput]
@PolymerRegister('main-app')
class MainApp extends PolymerElement {
  Logger _log = new Logger("MainApp");

  @Property(notify: true)
  String text;

  @Property(notify: true)
  String selected = 'tab1';

  @Property(notify: true)
  ScoringApplicationState state = new ScoringApplicationState();

  /// Constructor used to create instance of MainApp.
  MainApp.created() : super.created();

  @reflectable
  String reverseText(String text) {
    return text.split('').reversed.join('');
  }

  // Optional lifecycle methods - uncomment if needed.

  /// Called when an instance of main-app is inserted into the DOM.
  attached() {
    $['testDialog'].open();
  }

//  /// Called when an instance of main-app is removed from the DOM.
//  detached() {
//    super.detached();
//  }

//  /// Called when an attribute (such as a class) of an instance of
//  /// main-app is added, changed, or removed.
//  attributeChanged(String name, String oldValue, String newValue) {
//    super.attributeChanged(name, oldValue, newValue);
//  }

//  /// Called when main-app has been fully prepared (Shadow DOM created,
//  /// property observers set up, event listeners attached).
//  ready() {
//  }
}


class ScoringApplicationState {
  @Property(notify: true)
  Season selectedSeason;

  @Property(notify: true)
  Event selectedEvent;

  @Property(notify: true)
  Division selectedDivision;
}