library scoring_client.lib.services.rpc_service;

import 'package:logging/logging.dart';
import 'package:jsonrpc2/jsonrpc_client.dart';
import 'package:jsonrpc2/client_base.dart';
import 'dart:async';
import 'package:json_java_converter/json_java_converter.dart';


abstract class RpcService {
  final Logger _log = new Logger('RpcService');

  static const _remoteUrl = 'http://localhost:8090/'; // URL to root API

  final ServerProxy proxy;

  final RpcConverter converter;

  RpcService(String path)
    : proxy = new ServerProxy(_remoteUrl + path),
      converter = new RpcConverter();

  Future<dynamic> call(String method, [List<dynamic> args]) async {
    try {
      args = args?.map((a) => converter.convertToJson(a))?.toList();
      var response = await proxy.call(method, args);
      proxy.checkError(response);
      var ret = converter.convertFromJson(response);
      _log.finest(ret);
      return ret;
    } on RemoteException catch (e) {
      _log.warning(e);
      rethrow;
    } catch (e) {
      _log.severe(e);
      rethrow;
    }
  }

}