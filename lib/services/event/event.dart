import 'package:json_java_converter/json_java_converter.dart';
import 'package:scoring_client/services/season/season.dart';

@RemoteClass("com.maths22.ftc.entities.Event")
class Event {
  @remote
  String id;

  @remote
  String name;

  @remote
  DateTime startDate;

  @remote
  DateTime endDate;

  @remote
  EventType type;

  @remote
  Season season;

  @override
  String toString() {
    return 'Event{id: $id, name: $name, startDate: $startDate, endDate: $endDate, type: $type, season: $season}';
  }

}

@RemoteClass("com.maths22.ftc.entities.EventType")
class EventType {
  @remote
  String id;

  @remote
  String name;

  @override
  String toString() {
    return 'EventType{id: $id, name: $name}';
  }

}