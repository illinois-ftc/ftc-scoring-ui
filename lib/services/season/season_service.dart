import 'package:scoring_client/services/services.dart';
import 'package:scoring_client/services/rpc_service.dart';
import 'dart:async';

class SeasonService extends RpcService {
  static const _remotePath = 'seasonService'; // URL to web API

  SeasonService() : super(_remotePath);

  Future<List<Season>> getSeasons() => call("getSeasons");
  Future<Season> getSeason(String id) => call("getSeason", [id]);

}