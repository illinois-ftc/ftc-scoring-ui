import 'package:json_java_converter/json_java_converter.dart';

@RemoteClass("com.maths22.ftc.entities.Season")
class Season {
  @remote
  String id;

  @remote
  String name;

  @remote
  String slug;

  @remote
  String year;

  @override
  String toString() {
    return 'Season{id: $id, name: $name, slug: $slug, year: $year}';
  }


}