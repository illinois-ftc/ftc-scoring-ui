import 'package:json_java_converter/json_java_converter.dart';
import 'package:scoring_client/services/season/season.dart';
import 'package:scoring_client/services/event/event.dart';

@RemoteClass("com.maths22.ftc.entities.Division")
class Division {
  @remote
  String id;

  @remote
  String name;

  @remote
  Event event;

  @override
  String toString() {
    return 'Division{id: $id, name: $name, event: $event}';
  }

//TODO create matches
//  @remote
//  List<Match> matches;

  //TODO create team event assignments
//  @remote
//  List<TeamEventAssignment> teams;



}